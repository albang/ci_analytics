from datetime import datetime
import streamlit as st
from pathlib import Path

st.set_page_config(
    page_title="CIAnalitics",
    page_icon="👋",
)
st.title("CI Analytics")


Project_list = []
col1, col2 = st.columns(2)
with col1:
    st.image("./img/minion.png")
with col2:
    st.markdown("""
    CI Analitics est un projet qui interroge les API gitlab et génère des graphes avec plotly et streamlit. Il permet de mieux comprendre le comportement de la CI
    """)

for path in Path("./data/").iterdir():
    if path.name.startswith("project_"):
        Project_list.append(path)

col1, col2, col3 = st.columns(3)
try:
    p = Path("./data/project_all.json")
    dt = datetime.fromtimestamp(p.stat().st_mtime)
    col1.metric(label="Dernière mise à jour", value=dt.strftime('%d/%m/%Y'), delta="")
    col2.metric(label="heure", value=dt.strftime('%H:%M'), delta="")
    col3.metric(label="Projets instrumentés", value=len(Project_list), delta="")
except:
    st.markdown('<a href="./Synchronisation_gitlab">Synchronisation Gitlab</a>',unsafe_allow_html=True)
    st.warning("essayer de faire l'import de gitlab", icon="ℹ️")

