
import streamlit as st
from datetime import datetime, timedelta
import gitlab
import json
from dateutil import parser
import urllib3
from pathlib import Path
import tomllib
import os

DATA_PATH="./data/"

def get_jobs(project,status_bar):
    status_bar.progress(0, text=f"Début de l'importation")
    jobs_list = []
    already_existing_jobs = []
    try:
        with open(
            f"{DATA_PATH}project_{project.name}_{project.id}_jobs.json", "r"
        ) as project_cache:
            project_data = json.load(project_cache)
            if len(project_data["job_list"]):
                last_job_import = project_data["job_list"][0]["id"]
            else:
                last_job_import = 0
            already_existing_jobs = project_data["job_list"]
    except Exception as e:
        last_job_import = 0
        print(e)
    i = 0
    already_imported = False
    while True:
        try:
            job_list_tmp = project.jobs.list(page=i, per_page=20)
            if len(job_list_tmp) > 0:
                for job in job_list_tmp:
                    if job is None or job.id <= last_job_import:
                        print(f"Job déja importé: {job.id}")
                        already_imported = True
                        break
                    i+=1
                    status_bar.progress(i%100, text=f"Importation de {job.id}")
                    job_as_dict = job.asdict()
                    job_as_dict["name_with_namespace"] = project.name_with_namespace
                    jobs_list.append(job_as_dict)
                if already_imported:
                    break
            else:
                break
        except Exception:
            break
    with open(f"{DATA_PATH}project_{project.name}_{project.id}_jobs.json", "w") as jobs_out:
        json.dump(
            {
                "project_name": project.name_with_namespace,
                "job_list": jobs_list + already_existing_jobs,
            },
            jobs_out,
        )


st.set_page_config(
    page_title="CIAnalitics",
    page_icon="👋",
)
st.title("CI Analytics")


Project_list = []
col1, col2 = st.columns(2)
with col1:
    st.image("./img/minion.png")
with col2:
    st.sidebar.success("Select a demo above.")
    run_sync =  st.button("Lancer la synchro git")
    #btn_success = st.button("Clear cache")
    #st.text(os.listdir(f"./data/"))
if run_sync:
    st.warning("Merci de laisser la page ouverte jusqu'à la fin de l'import", icon="⚠️")

    with open("./config/config.toml", "rb") as f:
        config = tomllib.load(f)
        gitlab_url=os.environ.get("GITLAB_URL",config["gitlab"]["url"])
        gitlab_token = os.environ.get("GITLAB_TOKEN", config["gitlab"]["token"])
        gitlab_check_day = os.environ.get("GITLAB_CHECK_DAY", config["gitlab"]["check_day"])
    gl = gitlab.Gitlab(
        gitlab_url,
        ssl_verify=False,
        private_token=config["gitlab"]["token"],
    )
    gl.auth()
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    date_stop = datetime.now().replace(tzinfo=None)
    projects_list = []

    progress_text = "Operation in progress. Please wait."
    my_bar = st.progress(100, text=progress_text)
    percent_complete=0

    job_import = st.progress(0, text="import des jobs")
    percent_complete=0

    for i in range(0, 100):
        projects = gl.projects.list(
            sort_by="last_activity_at", sort="desc", page=i, per_page=20
        )
        for project in projects:
            percent_complete +=1
            my_bar.progress(percent_complete%100, text=f"Importation de {project.name}")
            print(project.last_activity_at)
            print(project.name)
            date_stop = parser.parse(project.last_activity_at).replace(tzinfo=None)
            without_tz = datetime.now()
            if without_tz - date_stop > timedelta(days=config["gitlab"]["check_day"]):
                exit_condition = True
            else:
                exit_condition = False
                projects_list.append(project.to_json())
                print(get_jobs(project,status_bar=job_import))
    my_bar.progress(100, text=f"Fin de l'importation des projects")
    job_import.progress(100, text=f"Fin des jobs")
    global_list = []
    j=0
    for i in Path(f"{DATA_PATH}").iterdir():
        if (
                i.name.endswith(".json")
                and i.name.startswith("project")
                and i.name != "project_all.json"
        ):
            j+=1
            job_import.progress(j%100, text=f"Chargement de {i.name} dans le cache global")
            with open(f"{DATA_PATH}{i.name}", "r") as data_in:
                global_list = global_list + json.load(data_in)["job_list"]

    with open(f"{DATA_PATH}project_all.json", "w") as out_file:
        job_import.progress(90, text=f"Ecriture du fichier sur disque ")
        json.dump(
            {"project_name": "all projects", "job_list": global_list},
            out_file,
            indent=4,
        )
        job_import.progress(100, text=f"Ecriture terminée ")
    st.balloons()

    st.info("Fin de L'import des données", icon="ℹ️")