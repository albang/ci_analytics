from datetime import datetime, timedelta
import streamlit as st

import json
import pandas as pd
import plotly.express as px


st.set_page_config(
    page_title="CIAnalytics",
    page_icon="👋",
)

DATA_PATH="./data/"
date_start = st.date_input("Date début")
date_fin = st.date_input("Date fin")
Project_list = []


with open(f"{DATA_PATH}project_all.json") as data_in:
    data = json.load(data_in)
    df = pd.DataFrame(data["job_list"])
try:
    filter = "failed"
    df = df[(df.status == filter)]
    df["started_at_datetime"] = pd.to_datetime(df["started_at"]).dt.tz_localize(None)
    if date_start:
        df = df[
            (
                df.started_at_datetime
                >= datetime.combine(date_start, datetime.min.time()) - timedelta(days=1)
            )
        ]
    if date_fin:
        df = df[
            (df.started_at_datetime <= datetime.combine(date_fin, datetime.max.time()))
        ]

    if filter == "failed":
        st.table(df.groupby(df["failure_reason"]).count().reset_index())
        runner_name_list = []
        for job in df.loc[:, "runner"]:
            if job:
                runner_name_list.append(job.get("description"))
            else:
                runner_name_list.append(None)
        df_runner = df.assign(runner_name=runner_name_list)  # Add new column
        user_name_list = []
        df_user = df

        with st.expander("Liste de tous les jobs en échec"):

            def make_clickable(link):
                # target _blank to open new window
                # extract clickable text to display for your link
                text = link
                return f'<a target="_blank" href="{link}">{text}</a>'

            df_runner["web_url"] = df["web_url"].apply(make_clickable)
            for runner in df_runner["runner_name"].unique():
                st.write(f"<br>{runner}<br>", unsafe_allow_html=True)
                st.write(
                    df_runner[(df_runner["runner_name"] == runner)][
                        ["runner_name", "failure_reason", "web_url"]
                    ].to_html(escape=False, index=False),
                    unsafe_allow_html=True,
                )

                # st.plotly_chart(table_error)

    df_stack = (
        df.groupby([df_runner["name_with_namespace"], df_runner["name"]])
        .size()
        .reset_index()
    )
    fig4 = px.bar(
        df_stack, x="name_with_namespace", y=df_stack[0], color="name", barmode="stack"
    )
    fig4.update_layout(
        title="Nombre de fail par project",
        xaxis_title="Projets",
        yaxis_title="Nombre de fails",
        xaxis_categoryorder="total descending",
        width=1200,
        height=600,
    )
    print(df_stack)

    #    fig4 = px.bar(y=failed_serie["name_with_namespace"].keys(), x=failed_serie["name_with_namespace"],color=failed_serie["name"])

    st.plotly_chart(fig4)
    #df["started_at"] = pd.to_datetime(df["started_at"], dayfirst=True)

    #for index in df_runner["runner_name"].unique():
    #    print(index)

    #graph_list = []
    #df = df.groupby([df["started_at"].dt.strftime("%H-%m")]).mean()

    #for user in df_user.loc[:, "user"]:
    #    if user:
    #        try:
    #            user_name_list.append(user.get("username"))
    #        except:
    #            user_name_list.append(None)
    #    else:
    #        user_name_list.append(None)
    #df_user = df_user.assign(username=user_name_list)  # Add new column
    #user_serie = (
    #    df_user[(df_user.status == "failed")].groupby(df_user["username"]).count()
    #)
    #print(user_serie["username"])
    #fig_user = px.bar(y=user_serie["username"].keys(), x=user_serie["username"])

    #my_layout = {
    #    "title": "Nombre de job failed by user ",
    #    "yaxis": {"title": "User", "categoryorder": "total ascending"},
    #    "xaxis": {
    #        "title": "Failed jobs",
    #    },
    #    "showlegend": True,
    #}
    #fig_user.update_layout(my_layout)
    #st.plotly_chart(fig_user)
except Exception as e:
    print(e)
    st.text("Nothing to show")
