from datetime import datetime, time, timedelta
import streamlit as st
from pathlib import Path
import json
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

st.set_page_config(
    page_title="CIAnalytics",
    page_icon="👋",
)

DATA_PATH="./data/"
Project_list = []
col1, col2 = st.columns(2)
with col1:
    st.image("./img/minion.png")
with col2:
    st.title("CI Analytics")
st.subheader("Status des jobs:")
col1, col2, col3 = st.columns(3)
with col1:
    btn_all = st.button("Status: All")
with col2:
    btn_success = st.button("Status: Success")
with col3:
    btn_failed = st.button("Status: failed")
st.subheader("Choix de la date:")
col1, col2 = st.columns(2)
with col1:
    date_start = st.date_input("Date début", datetime.today() - timedelta(days=7))
with col2:
    date_fin = st.date_input("Date fin")


Project_name_list = []
# YOLO
Project_path_list = []

for path in Path(f"{DATA_PATH}").iterdir():
    if path.name.startswith("project_"):
        with open(f"{DATA_PATH}{path.name}") as data_in:
            data = json.load(data_in)
        Project_name_list.append(data["project_name"])
        Project_path_list.append(f"{DATA_PATH}{path.name}")
st.subheader("Choix du projet:")
d = st.selectbox("Choisir le project gitlab", options=Project_name_list)
selection_index = Project_name_list.index(d)


with open(Project_path_list[selection_index]) as data_in:
    data = json.load(data_in)
    df = pd.DataFrame(data["job_list"])
try:
    if btn_success:
        filter = "success"
        df = df[(df.status == filter)]
    elif btn_failed:
        filter = "failed"
        df = df[(df.status == filter)]

    df["started_at_datetime"] = pd.to_datetime(df["started_at"]).dt.tz_localize(None)
    if date_start:
        df = df[
            (
                df.started_at_datetime
                >= datetime.combine(date_start, datetime.min.time()) - timedelta(days=1)
            )
        ]
    if date_fin:
        df = df[
            (df.started_at_datetime <= datetime.combine(date_fin, datetime.max.time()))
        ]

    df["timedelta"] = pd.to_timedelta(
        datetime.date(datetime.today()) - pd.to_datetime(df["started_at"]).dt.date
    ).dt.days

    col1, col2, col3 = st.columns(3)
    col1.metric(label="Nombre de jobs", value=len(df.count(axis=1)), delta="")

    df2 = df.groupby(by=["name"])["duration"].mean()

    job_by_duration = px.scatter(
        df,
        x="duration",
        y="name",
        color="stage",
        custom_data=["web_url", "status", "timedelta"],
    )
    job_by_duration.add_trace(
        go.Scatter(y=df2.keys(), x=df2, mode="markers", name="mean", fillcolor="yellow")
    )

    job_by_duration.update_traces(
        hovertemplate='<b>test</b><br><br><a href="%{customdata[0]}">Link</a><br>status: %{customdata[1]}<br>%{customdata[2]} jours'
    )
    job_by_duration_layout = {
        "title": "Durée des jobs par classés par nom",
        "yaxis": {"title": "jobs", "categoryorder": "total ascending"},
        "xaxis": {
            "title": "Durée d'éxécution",
        },
        "showlegend": True,
    }
    job_by_duration.update_layout(job_by_duration_layout)
    st.plotly_chart(job_by_duration)
    st.title("Statistique de réussite des jobs:")
    fig_pie = (
        df.status.str.get_dummies().sum()
    )  # .plot.pie(label='stage', autopct='%1.0f%%')

    fig_pie = px.pie(values=fig_pie, names=fig_pie.keys(), hole=0.3)
    st.plotly_chart(fig_pie)

    df["started_at_date"] = pd.to_datetime(df["started_at"]).dt.date
    dfg = df.groupby(df["started_at_date"]).count().reset_index()

    fig99 = px.bar(
        dfg,
        x="started_at_date",
        y="stage",
        title="Nombre de build par jour",
        color="id",
        barmode="stack",
    )
    st.plotly_chart(fig99)
    df1 = pd.DataFrame(df.groupby(by=["ref"])["duration"].sum())
    df1.reset_index(inplace=True)

    my_layout = {
        "title": "Somme de la durée des jobs par branche",
        "yaxis": {"title": "Publisher", "categoryorder": "total ascending"},
        "xaxis": {
            "title": "Views",
        },
        "showlegend": False,
    }
    fig2 = px.bar(df1, x="duration", y="ref", barmode="group", text="ref")
    fig2.update_layout(my_layout)
    st.plotly_chart(fig2)
    df2 = pd.DataFrame(df.groupby(by=["stage", "name"])["duration"].mean())
    df2.reset_index(inplace=True)
    my_layout = {
        "title": "Temps moyen consommé par job",
        "yaxis": {"title": "jobs", "categoryorder": "total ascending"},
        "xaxis": {
            "title": "Durée d'éxécution",
        },
        "showlegend": False,
    }
    fig3 = px.bar(
        df2, x="duration", y="name", barmode="group", text="name", color="stage"
    )
    fig3.update_layout(my_layout)
    st.plotly_chart(fig3)

    runner_name_list = []
    for job in df.loc[:, "runner"]:
        if job:
            runner_name_list.append(job.get("description"))
        else:
            runner_name_list.append(None)
    df_runner = df.assign(runner_name=runner_name_list)  # Add new column
    user_name_list = []
    df_user = df

    if filter == "failed":

        def make_clickable(link):
            # target _blank to open new window
            # extract clickable text to display for your link
            text = link
            return f'<a target="_blank" href="{link}">{text}</a>'

        df_runner["web_url"] = df["web_url"].apply(make_clickable)
        for runner in df_runner["runner_name"].unique():
            st.write(
                df_runner[(df_runner["runner_name"] == runner)][
                    ["runner_name", "failure_reason", "web_url"]
                ].to_html(escape=False, index=False),
                unsafe_allow_html=True,
            )

    fig4 = px.scatter(df_runner, x="started_at", y="duration", color="runner_name")
    my_layout = {
        "title": "Temps de build selon le temps de lancement par le runner ",
    }
    fig4.update_layout(my_layout)
    st.plotly_chart(fig4)
    df["started_at"] = pd.to_datetime(df["started_at"], dayfirst=True)

    for index in df_runner["runner_name"].unique():
        print(index)

    graph_list = []

    df = df.groupby([df["started_at"].dt.strftime("%H-%m")]).mean()


    #st.header("This is a USER")
    #for user in df_user.loc[:, "user"]:
    #    if user:
    #        try:
    #            user_name_list.append(user.get("username"))
    #        except:
    #            user_name_list.append(None)
    #    else:
    #        user_name_list.append(None)
    #df_user = df_user.assign(username=user_name_list)  # Add new column
    #user_serie = (
    #    df_user[(df_user.status == "failed")].groupby(df_user["username"]).count()
    #)
    #print(user_serie["username"])
    #fig_user = px.bar(y=user_serie["username"].keys(), x=user_serie["username"])

    #my_layout = {
    #    "title": "Nombre de job failed by user ",
    #    "yaxis": {"title": "User", "categoryorder": "total ascending"},
    #    "xaxis": {
    #        "title": "Failed jobs",
    #    },
    #    "showlegend": True,
    #}
    #fig_user.update_layout(my_layout)
    #st.plotly_chart(fig_user)
except Exception as e:
    print(e)
    st.text("Nothing to show")
