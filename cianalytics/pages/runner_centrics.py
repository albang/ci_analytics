

from datetime import datetime, time, timedelta
import streamlit as st
from pathlib import Path
import json
import pandas as pd
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objects as go

st.set_page_config(
    page_title="CIAnalitics",
    page_icon="👋",
)

DATA_PATH="./data/"

date_start = st.date_input("Date début")
date_fin = st.date_input("Date fin")
Project_list = []


for path in Path(f"{DATA_PATH}").iterdir():
    if path.name.startswith("project_"):
        Project_list.append(path)

d = st.selectbox("Choisir le project gitlab", options=Project_list)

with open(d) as data_in:
    data = json.load(data_in)
    df = pd.DataFrame(data["job_list"])
try:
    filter ="failed"
    df = df[(df.status == filter)]
    df["started_at_datetime"] = pd.to_datetime(df["started_at"]).dt.tz_localize(None)
    if date_start:
        df = df[
            (
                df.started_at_datetime
                >= datetime.combine(date_start, datetime.min.time()) - timedelta(days=1)
            )
        ]
    if date_fin:
        df = df[
            (df.started_at_datetime <= datetime.combine(date_fin, datetime.max.time()))
        ]

    if filter == "failed":
        st.table(df.groupby(df["failure_reason"]).count().reset_index())

    df["timedelta"] = pd.to_timedelta(
        datetime.date(datetime.today()) - pd.to_datetime(df["started_at"]).dt.date
    ).dt.days
    job_by_duration = px.scatter(
        df,
        x="duration",
        y="name",
        color="stage",
        custom_data=["web_url", "status", "timedelta"],
    )
    job_by_duration.update_traces(
        hovertemplate='<b>test</b><br><br><a href="%{customdata[0]}">Link</a><br>status: %{customdata[1]}<br>%{customdata[2]} jours'
    )
    job_by_duration_layout = {
        "title": "Job du projet classé par durée",
        "yaxis": {
            "title": "Temps de la CI",
        },
        "xaxis": {
            "title": "Jobs",
        },
        "showlegend": True,
    }

    st.plotly_chart(job_by_duration)



    df["started_at_date"] = pd.to_datetime(df["started_at"]).dt.date
    dfg = df.groupby(df["started_at_date"]).count().reset_index()

    fig99 = px.bar(
        dfg,
        x="started_at_date",
        y="stage",
        title="Nombre de build par jour",
        color="id",
        barmode="stack",
    )
    st.plotly_chart(fig99)
    df1 = pd.DataFrame(df.groupby(by=["ref"])["duration"].sum())
    df1.reset_index(inplace=True)

    my_layout = {
        "title": "Somme de la CI consommé  par branche",
        "yaxis": {"title": "Publisher", "categoryorder": "total ascending"},
        "xaxis": {
            "title": "Views",
        },
        "showlegend": False,
    }
    fig2 = px.bar(df1, x="duration", y="ref", barmode="group", text="ref")
    fig2.update_layout(my_layout)
    st.plotly_chart(fig2)
    df2 = pd.DataFrame(df.groupby(by=["stage", "name"])["duration"].mean())
    df2.reset_index(inplace=True)
    my_layout = {
        "title": "Temps moyen consommé par ",
        "yaxis": {"title": "Publisher", "categoryorder": "total ascending"},
        "xaxis": {
            "title": "Views",
        },
        "showlegend": False,
    }
    fig3 = px.bar(
        df2, x="duration", y="name", barmode="group", text="name", color="stage"
    )
    fig3.update_layout(my_layout)
    st.plotly_chart(fig3)
    runner_name_list = []
    for job in df.loc[:, "runner"]:
        if job:
            runner_name_list.append(job.get("description"))
        else:
            runner_name_list.append(None)
    df_runner = df.assign(runner_name=runner_name_list)  # Add new column
    user_name_list = []
    df_user = df



    if filter == "failed":
        def make_clickable(link):
            # target _blank to open new window
            # extract clickable text to display for your link
            text = link
            return f'<a target="_blank" href="{link}">{text}</a>'
        df_runner["web_url"] = df["web_url"].apply(make_clickable)
        for runner in df_runner["runner_name"].unique():
            st.write(
                df_runner[(df_runner["runner_name"] == runner)][
                    ["runner_name", "failure_reason", "web_url"]
                ].to_html(escape=False, index=False),
                unsafe_allow_html=True,
            )
    fig4 = px.scatter(df_runner, x="started_at", y="duration", color="runner_name")
    my_layout = {
        "title": "Temps de build selon le temps de lancement par le runner ",
    }
    fig4.update_layout(my_layout)
    st.plotly_chart(fig4)
    df["started_at"] = pd.to_datetime(df["started_at"], dayfirst=True)

    for index in df_runner["runner_name"].unique():
        print(index)

    graph_list = []

    df = df.groupby([df["started_at"].dt.strftime("%H-%m")]).mean()
    fig_pie = (
        df_runner.status.str.get_dummies().sum()
    )
    fig_pie = px.pie(values=fig_pie, names=fig_pie.keys(), hole=0.3)
    st.plotly_chart(fig_pie)
    for user in df_user.loc[:, "user"]:
        if user:
            try:
                user_name_list.append(user.get("username"))
            except:
                user_name_list.append(None)
        else:
            user_name_list.append(None)
    df_user = df_user.assign(username=user_name_list)  # Add new column
    user_serie = (
        df_user[(df_user.status == "failed")].groupby(df_user["username"]).count()
    )
    print(user_serie["username"])
    fig_user = px.bar(y=user_serie["username"].keys(), x=user_serie["username"])

    my_layout = {
        "title": "Nombre de job failed by user ",
        "yaxis": {"title": "User", "categoryorder": "total ascending"},
        "xaxis": {
            "title": "Failed jobs",
        },
        "showlegend": True,
    }
    fig_user.update_layout(my_layout)
    st.plotly_chart(fig_user)
except Exception as e:
    print(e)
    st.text("Nothing to show")
